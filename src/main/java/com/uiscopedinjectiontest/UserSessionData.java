package com.uiscopedinjectiontest;

import com.vaadin.cdi.UIScoped;

import java.io.Serializable;

@UIScoped
public class UserSessionData implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
