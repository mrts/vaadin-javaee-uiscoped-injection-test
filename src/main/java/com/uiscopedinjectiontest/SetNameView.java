package com.uiscopedinjectiontest;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.inject.Inject;

@CDIView(SetNameView.NAME)
public class SetNameView extends VerticalLayout implements View {

    public static final String NAME = "set-name";

    @Inject
    private UserSessionData userSessionData;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        final TextField name = new TextField();
        name.setCaption("Type your name here:");

        final Button setNameAndNavigateToShow = new Button("Click Me");
        setNameAndNavigateToShow.addClickListener(e -> {
            userSessionData.setName(name.getValue());
            getUI().getNavigator().navigateTo(ShowNameView.NAME);
        });

        addComponents(name, setNameAndNavigateToShow);
    }

}
