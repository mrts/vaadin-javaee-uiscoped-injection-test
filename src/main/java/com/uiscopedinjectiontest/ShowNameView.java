package com.uiscopedinjectiontest;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.inject.Inject;

@CDIView(ShowNameView.NAME)
public class ShowNameView extends VerticalLayout implements View {

    public static final String NAME = "show-name";

    @Inject
    private UserSessionData userSessionData;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        addComponent(new Label("Thanks " + userSessionData.getName() + ", it works!"));
    }

}
