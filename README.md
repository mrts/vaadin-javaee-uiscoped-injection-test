vaadin-javaee-uiscoped-injection-test
=====================================

A simple Vaadin 8 and Java EE application that demonstrates
`@UIScoped` injection along with `@Push`.
